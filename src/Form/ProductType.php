<?php

namespace App\Form;

use App\Entity\Product;
use App\Form\Type\DateTimePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('orderCode', IntegerType::class, [
                'attr' => ['autofocus' => true],
            ])
            ->add('productId', IntegerType::class, [

            ])
            ->add('quantity', IntegerType::class, [

            ])
            ->add('address', TextareaType::class, [

            ])
            ->add('shippingDate', DateTimePickerType::class, [

            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
