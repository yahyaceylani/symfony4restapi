<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Product Controller
 *
 * @Version("v1")
 */
class ProductController extends FOSRestController
{

    /**
     * Create Product.
     *
     * @FOSRest\Post("/products")
     *
     * @param AdapterInterface $cache
     * @param ConstraintViolationListInterface $validationErrors
     * @param EntityManagerInterface $em
     * @param EventDispatcherInterface $dispatcher
     * @param Product $product
     * @param Request $request
     * @param UrlGeneratorInterface $router
     *
     * @return View
     * @ParamConverter("product", converter="fos_rest.request_body")
     */
    public function postProduct(
        AdapterInterface $cache,
        ConstraintViolationListInterface $validationErrors,
        EntityManagerInterface $em,
        EventDispatcherInterface $dispatcher,
        Product $product,
        Request $request,
        UrlGeneratorInterface $router
    ): View {

        if (count($validationErrors) > 0) {
            return View::create(array('errors' => $validationErrors), Response::HTTP_BAD_REQUEST);
        }

        $em->persist($product);
        $em->flush();

        $dispatcher->dispatch($product);

        $response = array(
            'id' => $product->getId(),
            'name' => $product->getName(),
            'description' => $product->getDescription(),
            'date' => $product->getDateTime(),
            'url' => $router->generate(
                'api_product_index',
                array('id' => $product->getId(), 'version' => 'v1')
            )
        );
        return View::create($product, Response::HTTP_CREATED, []);
    }

    /**
     * Lists all Products.
     * @FOSRest\Get("/products")
     * @QueryParam(name="search", requirements="[a-z]+", description="search", allowBlank=false)
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="limit", requirements="\d+", default="5", description="How many notes to return.")
     * @QueryParam(name="sort", requirements="(asc|desc)", allowBlank=false, default="desc", description="Sort direction")
     *
     * @param AdapterInterface $cache
     * @param ProductRepository $productRepository
     * @param MessageBusInterface $bus
     * @param ParamFetcherInterface $paramFetcher
     * @return JsonResponse
     */
    public function getProducts(
        AdapterInterface $cache,
        ProductRepository $productRepository,
        MessageBusInterface $bus,
        ParamFetcherInterface $paramFetcher
    ): JsonResponse
    {
        $repository = $this->getDoctrine()->getRepository(Product::class);
        // add pagination on data using ParamFetcherInterface
        $limit = $paramFetcher->get('limit');
        $page = $limit * ($paramFetcher->get('page') - 1);
        $products = $repository->findBy(array(), array('id' => $paramFetcher->get('sort')), $limit, $page);

        return new JsonResponse(array(
            '_links' => array(
                'next' => sprintf('/products?limit=%d&page=%d', $limit, $paramFetcher->get('page')  + 1),
                'prev' => sprintf('/products?limit=%d&page=%d', $limit, $paramFetcher->get('page')  - 1),
            ),
            'limit' => $limit,
            'results' => $products,
            'size' => (int) $limit,
            'start' => $page
        ), Response::HTTP_OK, []);
    }

    /**
     * Get Product.
     * @FOSRest\Get(path = "/products/{id}", name="product_index")
     * @param $id
     * @param ProductRepository $productRepository
     * @return View
     */
    public function getProduct($id, ProductRepository $productRepository): View
    {
        // query for a single Product by its primary key (usually "id")
        $product = $productRepository->find($id);
        if (!$product) {
            throw new HttpException(404, 'Product not found');
        }
        // Move this in Product normalizer
        $response = array(
            'id' => $product->getId(),
            'orderCode' => $product->getOrderCode(),
            'productId' => $product->getProductId(),
            'quantity' => $product->getQuantity(),
            'address' => $product->getAddress(),
            'shippingDate' => $product->getShippingDate()
        );

        return View::create($product, Response::HTTP_OK);
    }

    /**
     * Update an Product.
     * @FOSRest\Put(path = "/products/{id}")
     *
     * @param $id
     * @param Request $request
     * @param ProductRepository $productRepository
     * @param EntityManagerInterface $em
     * @return View
     * @throws Exception
     */
    public function putProduct($id, Request $request, ProductRepository $productRepository, EntityManagerInterface $em): View
    {
        $product = $productRepository->find($id);
        if (!$product) {
            throw new HttpException(404, 'Product not found');
        }
        $postData = json_decode($request->getContent(), true);

        if (new \DateTime($postData->shippingDate) < (new \DateTime())->format('Y-m-d H:i:s')) {
            throw new MethodNotAllowedException([]);
        }

        $product->setOrderCode($postData->orderCode);
        $product->setProductId($postData->productId);
        $product->setQuantity($postData->quantity);
        $product->setAddress($postData->address);
        $product->setShippingDate(new \DateTime($postData->shippingDate));

        $em->persist($product);
        $em->flush();
        return View::create($product, Response::HTTP_OK, []);
    }

    /**
     * Delete an Product.
     *
     * @FOSRest\Delete(path = "/products/{id}")
     * @param $id
     * @param Request $request
     * @param ProductRepository $productRepository
     * @param EntityManagerInterface $em
     * @return View
     */
    public function deleteProduct($id, Request $request, ProductRepository $productRepository, EntityManagerInterface $em): View
    {
        $product = $productRepository->find($id);
        if (!$product) {
            throw new HttpException(404, 'Product not found');
        }
        $em->remove($product);
        $em->flush();
        return View::create(null, Response::HTTP_NO_CONTENT);
    }
}
